export function matchStateToTerm(state, value) {
  return (
    state.name.toLowerCase().indexOf(value.toLowerCase()) !== -1 ||
    state.abbr.toLowerCase().indexOf(value.toLowerCase()) !== -1
  )
}

export function fakeRequest(value, cb) {
  return setTimeout(cb, 500, value ?
    getStates().filter(state => matchStateToTerm(state, value)) :
    getStates()
  )
}

export function getStates() {
  return [
    { abbr: 'AL', name: 'Adelaide (ADL)' },
    { abbr: 'AK', name: 'Alicante (ALC)' },
    { abbr: 'AZ', name: 'Athen (ATH)' },
    { abbr: 'AR', name: 'Accra (ACC)' },
    { abbr: 'CA', name: 'Korsika / Ajaccio (AJA)' },
    { abbr: 'CO', name: 'Amsterdam (AMS)' },
    { abbr: 'CT', name: 'Atlanta (ATL)' },
    { abbr: 'DE', name: 'Abu Dhabi (AUH)' },
    { abbr: 'FL', name: 'Auckland (AKL)' },
    { abbr: 'GA', name: 'Buenos Aires (BUE)' },
    { abbr: 'HI', name: 'Los Angeles (LAX)' },
  ]    
}

