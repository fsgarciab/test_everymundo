import {fakeRequest} from '../../helpers/utils';
export const SUGGESTIONS_SEARCH_INIT = 'SUGGESTIONS_SEARCH_INIT';
export const SUGGESTIONS_SEARCH_FINISHED = 'SUGGESTIONS_SEARCH_FINISHED';


export const REDIRECT_SEARCH_INIT = 'REDIRECT_SEARCH_INIT';



function redirectSearchInit() {
  return {type: REDIRECT_SEARCH_INIT}
};

function getSuggestionsSearchInit() {
  return {type: SUGGESTIONS_SEARCH_INIT}
};

function getSuggestionsSearchFinished(data) {
  return {
  	type: SUGGESTIONS_SEARCH_FINISHED,
  	payload: data
  	}
};


export function redirectProducts(url) {
  return function (dispatch){
  	dispatch(redirectSearchInit());
  	window.location.href = url;
  }
}

export function getSuggestions(search){
	return function(dispatch){
		dispatch(getSuggestionsSearchInit());
		fakeRequest(search, (items) => {
              dispatch(getSuggestionsSearchFinished(items));
        })
	}
}