
import React from 'react';
import { shallow, mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import FlightComponent from '../FlightComponent';
import { InputCoupleOriginDestiny , InputCoupleDatePickerInitFinish, InputSelectPassengers } from '../../commons/inputs';

const mockStore = configureStore([]);
const store = mockStore({flightData:{
  isLoading:false,
  data:[
    {abbr: "AL", name: "Adelaide (ADL)"},

    {abbr: "AK", name: "Alicante (ALC)"},

    {abbr: "AZ", name: "Athen (ATH)"},

    {abbr: "AR", name: "Accra (ACC)"},

    {abbr: "CA", name: "Korsika / Ajaccio (AJA)"},

    {abbr: "CO", name: "Amsterdam (AMS)"},

    {abbr: "CT", name: "Atlanta (ATL)"},

    {abbr: "DE", name: "Abu Dhabi (AUH)"},

    {abbr: "FL", name: "Auckland (AKL)"},

    {abbr: "GA", name: "Buenos Aires (BUE)"},

    {abbr: "HI", name: "Los Angeles (LAX)"},
  ]
},
form:{
  FlightComponent:{ 
    values:{
      endDate:"2018-01-17",
      flight_toggle:true,
      from:"Alicante (ALC)",
      passengers_adults:1,
      passengers_children:2,
      passengers_infants:0,
      startDate:"2018-01-09",
      to:"Accra (ACC)",
    }
}
}
});
let wrapper;
describe('components', () => {
  describe('BookingBarContainer', () => {

  beforeEach(() => {
      wrapper = mount(
        <Provider store={store}>
            <FlightComponent />
        </Provider>
      );
  });
    it('should render self ', () => {
     

      expect(wrapper.contains(InputCoupleOriginDestiny)).toEqual(true);

      expect(wrapper.contains(InputCoupleDatePickerInitFinish)).toEqual(true);

      expect(wrapper.contains(InputSelectPassengers)).toEqual(true);

    })
  })
})

