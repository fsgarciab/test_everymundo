import React, { Component } from 'react';
import { Field, reduxForm, formValueSelector, change } from 'redux-form';
import { InputCoupleOriginDestiny , InputCoupleDatePickerInitFinish,InputText, InputSelectPassengers } from '../commons/inputs';
import {ButtonSubmit} from '../commons/buttons';
import { connect } from 'react-redux';
import { redirectProducts, getSuggestions } from './FlightActions';
import './FlightComponent.css'


class FlightComponent extends Component{
	handleSubmit(values){
		const {url, dispatch} = this.props
        dispatch(redirectProducts(url));
	}
	render(){
		let {startDate,endDate,dispatch, flight_toggle, passengers_adults, passengers_children, passengers_infants, handleSubmit, getSuggestionsBySearch,items, changeValue} =this.props;
		return (
				<form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="flightComponent">

			         <div className=" em-display-flex">
						<InputCoupleOriginDestiny items={items} getSuggestionsBySearch={getSuggestionsBySearch}  style={{width:'38%',marginRight:'20px'}}/>

						<InputCoupleDatePickerInitFinish style={{width:'38%',marginRight:'20px'}} formName="FlightComponent" startDate={startDate} endDate={endDate} flight_toggle={flight_toggle} changeValue={changeValue}/>
					
						<InputSelectPassengers style={{minWidth:'23%'}} formName="FlightComponent" passengers_adults={passengers_adults} passengers_children={passengers_children} passengers_infants={passengers_infants} changeValue={changeValue}/>
				    </div>
                    <div className=" em-display-flex" style={{paddingTop:'20px'}}>
                        <div className="em-booking_bar--links">
                            <ul className="em-link_list em-link_list-inline">
                                <li>  
                                    <a className="em-link evo-link-arrow" href="https://www.swiss.com/us/en/Book/Flight">
                                        Advanced search
                                    </a>   
                                </li>
                                <li>
                                    <a className="em-link evo-link-arrow" href="https://www.swiss.com/us/en/book/flight-information/arrivals-and-departures">
                                        Arrivals and departures
                                    </a>
                                </li>
                                <li>                      
                                    <a className="em-link evo-link-arrow" href="https://www.swiss.com/Explore/en/destination-finder">
                                        Inspire me
                                    </a>
                                </li>
                                <li>             
                                    <a className="em-link evo-link-arrow" href="https://www.swiss.com/us/en/various/miles-and-more">
                                       Miles &amp; More
                                    </a>   
                                </li>
                            </ul>
                           
                        </div>

                        <div className="em-booking_bar_submit">
                            <ButtonSubmit >
                                Search
                            </ButtonSubmit>
                        </div>
                    </div>
				</form>
			)
	}
}


FlightComponent = reduxForm({
  form: 'FlightComponent'
})(FlightComponent);

const selector = formValueSelector('FlightComponent');


const mapStateToProps = (state)=>{
    let from_origin = selector(state, 'from');
    let to_destiny = selector(state, 'to');
    let startDate = selector(state, 'startDate');
    let endDate = selector(state, 'endDate');
    let flight_toggle = selector(state, 'flight_toggle');
    let passengers_adults = selector(state, 'passengers_adults');
    let passengers_children = selector(state, 'passengers_children');
    let passengers_infants = selector(state, 'passengers_infants');

    let regexp_places = /\(([^)]+)\)/;
    let from_code = regexp_places.exec(from_origin);
    let to_code = regexp_places.exec(to_destiny);
    let url='https://www.swiss.com/us/en/Book/Outbound/';
    if(from_code){
        url += from_code[1] + '-';
    }
    if(to_code){
        url += to_code[1] + '/';
    }
    if(startDate){
        url += 'from-' + startDate+ '/';
    }
    if(endDate){
        url += 'to-' + endDate + '/';
    }
    if(passengers_adults!=null){
        url += 'adults-' + passengers_adults + '/';
    }
    if(passengers_children!=null){
        url += 'children-' + passengers_children + '/';
    }
    
    if(passengers_infants!=null){
        url += 'infants-' + passengers_infants + '/';
    }

    url += 'class-economy/al-LX/sid5goh' ;  

    let items = []
    if(!state.flightData.isLoading){
        items = state.flightData.data;
    }



    return {
        startDate,
        endDate,
        flight_toggle,
        passengers_adults,
        passengers_children,
        passengers_infants,
        url,
        items,
        initialValues:{
            flight_toggle:true,
            passengers_adults:1,
            passengers_children:0,
            passengers_infants:0
        }
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getSuggestionsBySearch: (search)=>{
            dispatch(getSuggestions(search));
        },
        changeValue : (formName,fieldName,value)=>{
            dispatch(change(formName,fieldName,value));
        }
    }
}

FlightComponent = connect(mapStateToProps,mapDispatchToProps)(FlightComponent);

export default FlightComponent;