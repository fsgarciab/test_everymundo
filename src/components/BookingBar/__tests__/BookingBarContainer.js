
import React from 'react';
import { mount, shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk';
import BookingBarContainer from '../BookingBarContainer';
import { TabsPanel } from '../../commons/tabs';
import FlightComponent from '../../Flight/FlightComponent';


describe('components', () => {
  describe('BookingBarContainer', () => {
    it('should render self ', () => {
      const wrapper = shallow(<BookingBarContainer />);
      const icon_1 = <span className="icon icon-flight">&nbsp;</span>;
      expect(wrapper.contains(icon_1)).toEqual(true);
      const icon_2 = <span className="icon icon-hotal">&nbsp;</span>;
      expect(wrapper.contains(icon_1)).toEqual(true);
      const icon_3 = <span className="icon icon-rentalcar">&nbsp;</span>;
      expect(wrapper.contains(icon_1)).toEqual(true);
      const icon_4 = <span className="icon icon-swiss">&nbsp;</span>;
      expect(wrapper.contains(icon_1)).toEqual(true);


      const flight_component = <FlightComponent />;
      expect(wrapper.contains(flight_component)).toEqual(true);
    })
  })
})