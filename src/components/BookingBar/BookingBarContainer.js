import React , {Component} from 'react';
import { Tab, TabList, TabPanel } from 'react-tabs';
import { TabsPanel } from '../commons/tabs';
import FlightComponent from '../Flight/FlightComponent';
import './BookingBarContainer.css'

class BookingbarContainer extends Component{

	render(){

		return (
			<TabsPanel className="bookingbarContainer">
			    <TabList>
			      	<Tab> <span className="icon icon-flight">&nbsp;</span> Flight</Tab>
			      	<Tab> <span className="icon icon-hotel">&nbsp;</span> Hotel</Tab>
			        <Tab> <span className="icon icon-rentalcar">&nbsp;</span>Rental Car</Tab>
			        <Tab> <span className="icon icon-swiss">&nbsp;</span> Swiss algo</Tab>
			    </TabList>
			 
			    <TabPanel>
			      	<FlightComponent />
			    </TabPanel>

			    <TabPanel>
			      <h2>Any content 2</h2>
			    </TabPanel>

			    <TabPanel>
			      <h2>Any content 3</h2>
			    </TabPanel>

			    <TabPanel>
			      <h2>Any content 4</h2>
			    </TabPanel>
		  	</TabsPanel>
			)
		
	}
}
export default BookingbarContainer;
