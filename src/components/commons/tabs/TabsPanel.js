import React , {Component} from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './TabsPanel.css';

class TabsPanel extends Component{

	render(){
		return (
			<Tabs className={'react-tabs '+this.props.className}>
			    {this.props.children}
		  	</Tabs>
			)
		
	}
}
export  {TabsPanel};
