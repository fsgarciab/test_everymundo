import React , {Component} from 'react';
import './ButtonSubmit.css';

class ButtonSubmit extends Component{
	render(){
		const {onClick, style} = this.props;
		return (
			<button className="buttonSubmit" onClick={onClick} style={style} type="submit">
           		{this.props.children}
        	</button>  
			)
	}
}
export {ButtonSubmit};