import React from 'react'
import { mount } from 'enzyme'
import {ButtonControl,ButtonSubmit} from '../';

function setup() {
  const props = {
    onClick: jest.fn()
  }

  const enzymeWrapper_buttonControl = mount(<ButtonControl {...props} />)

  const enzymeWrapper_buttonSubmit = mount(<ButtonSubmit {...props} />)


  return {
    props,
    enzymeWrapper_buttonControl,
    enzymeWrapper_buttonSubmit
  }
}

describe('components', () => {
  describe('ButtonControl', () => {
    it('should render self ', () => {
      const { enzymeWrapper_buttonControl } = setup()
      expect(enzymeWrapper_buttonControl.find('button').hasClass('buttonControl')).toBe(true)
      const todoInputProps = enzymeWrapper_buttonControl.find('button').props()
      expect(todoInputProps.type).toEqual('button')
    })

  })
  

    describe('ButtonSubmit', () => {
    it('should render self and subcomponents', () => {
      const { enzymeWrapper_buttonSubmit } = setup()
      expect(enzymeWrapper_buttonSubmit.find('button').hasClass('buttonSubmit')).toBe(true)
      const todoInputProps = enzymeWrapper_buttonSubmit.find('button').props()
      expect(todoInputProps.type).toEqual('submit')
    })
 
  })
})