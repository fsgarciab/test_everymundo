import React, {Component} from 'react';
import './ButtonControl.css';

class ButtonControl extends Component{
	render(){
		const {onClick, style} = this.props;
		 return (
		 	<button className="buttonControl" onClick={onClick} style={style} type="button">
           		{this.props.children}
        	</button>  
        	)
	}
}

export {ButtonControl}