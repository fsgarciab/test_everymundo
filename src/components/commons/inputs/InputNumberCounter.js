import React, {Component} from 'react';
import {Field,change} from 'redux-form';
import './InputNumberCounter.css';
import {ButtonControl} from '../buttons';


class InputNumberCounter extends Component{
	constructor(props){
		super(props);
		this.increment = this.increment.bind(this);
		this.decrement = this.decrement.bind(this);
	}
	increment(){
		const {changeValue, selector, formName, name, max} = this.props;
			if(selector<max){
				changeValue(formName,name,selector + 1);
			}
			
	}

	decrement(){
		const {changeValue, selector, formName, name, min} = this.props;
			if(selector>min){
				changeValue(formName,name,selector - 1);
			}
	}


	render(){
		const { label, sublabel , name, formName} = this.props;
		return (
				<div className="inputNumberCounter">
                    <label className="evo-input--label">
                        {label}
                        <small>{sublabel}</small>
                    </label>
                    <div className="em-input-number" >
                       	<ButtonControl style={{right:'-1px'}} onClick={this.decrement} >-</ButtonControl>                              
                        <Field name={name} component="input" type="number"/>
                        <ButtonControl style={{left:'-1px'}} onClick={this.increment} >+</ButtonControl> 
                    </div>
                </div>
			)
	}

}


export {InputNumberCounter};
