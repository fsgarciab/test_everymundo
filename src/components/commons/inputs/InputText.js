import React, {Component} from 'react';
import './InputText.css';


class InputText extends Component{
	constructor(props) {
	  super(props);
	}

	render (){

		let { className, input, label, onfocus , disabled} = this.props;
		let classes = [];
		classes.push(className);
		if (input.value.length>0){
			classes.push("has-value");
		}

		return (
			<div className={ ['inputText', ...classes].join(' ')}>
				<input {...input} type="text" onFocus={onfocus} disabled={disabled}/>
				<label>{label}</label>
			</div>
			)
	}

}

export {InputText};