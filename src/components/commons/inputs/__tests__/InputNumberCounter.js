import React from 'react'
import { shallow,mount } from 'enzyme'
import {InputNumberCounter} from '../';
import {ButtonControl} from '../../buttons';
import {Field} from 'redux-form';
function setup() {
  const props = {
    label: 'Label Test',
    sublabel:'Sub Label Test',
    name: 'name Test',
    formName: 'formName Test',
    changeValue: jest.fn(),
    selector: 1,
    min:1,
    max:5
  }

  const enzymeWrapper = shallow(<InputNumberCounter {...props} />)


  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('InputNumberCounter', () => {
    it('should render self ', () => {
      const { enzymeWrapper } = setup()
      expect(enzymeWrapper.find(ButtonControl)).toHaveLength(2);
      expect(enzymeWrapper.find(Field)).toHaveLength(1);

    })
  })

})