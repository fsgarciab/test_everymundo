import React from 'react'
import { shallow,mount } from 'enzyme'
import {InputFlightToggle} from '../';
function setup() {
  const props = {
    input: {
      value:true,
      onChange:jest.fn(),
       onBlur:jest.fn,
    },
    label: 'Label Test',
    onfocus: jest.fn(),
    className : 'testClass'
    }

  const enzymeWrapper = shallow(<InputFlightToggle {...props} />)


  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('InputNumberCounter', () => {
    it('should render self ', () => {
      const { enzymeWrapper } = setup()
      expect(enzymeWrapper.find('div').first().hasClass('inputFlightToggle')).toBe(true);
      expect(enzymeWrapper.find('div').first().hasClass('testClass')).toBe(true);
      expect(enzymeWrapper.find('input')).toHaveLength(1);
      const todoInputProps = enzymeWrapper.find('input').props();
      expect(todoInputProps.type).toEqual('checkbox');
    })
  })

})