import React from 'react'
import { shallow,mount } from 'enzyme'
import {InputCoupleDatePickerInitFinish} from '../';
import {DayPickerRangeController , DayPickerSingleDateController} from 'react-dates';
import {Field} from 'redux-form';
function setup() {
  const props = {

    style:{padding:"10px"},
    changeValue: jest.fn(),
    formName: "FormNameTest",
    flight_toggle:true
    }

  const enzymeWrapper = shallow(<InputCoupleDatePickerInitFinish {...props} />)


  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('InputCoupleDatePickerInitFinish', () => {
    it('should render self ', () => {
      const { enzymeWrapper } = setup()
      expect(enzymeWrapper.find('div').first().hasClass('inputCoupleDatePickerInitFinish')).toBe(true);
      expect(enzymeWrapper.find(DayPickerRangeController)).toHaveLength(0);
     
    
    })
  })

})