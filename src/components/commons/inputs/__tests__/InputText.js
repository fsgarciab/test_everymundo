import React from 'react'
import { mount } from 'enzyme'
import {InputText} from '../';

function setup() {
  const props = {
    input: {
      value:'dasd',
      onChange:jest.fn(),
       onBlur:jest.fn,
    },
    label: 'Label Test',
    items: [{name:'Arcansas (ARC)'}],
    getSuggestionsBySearch: jest.fn(),
    onfocus: jest.fn(),

  }

  const enzymeWrapper = mount(<InputText {...props} />)



  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('InputText', () => {
    it('should render self and subcomponets', () => {
      const { enzymeWrapper } = setup()
      expect(enzymeWrapper.find('div').hasClass('inputText')).toBe(true);
      const todoInputProps = enzymeWrapper.find('input').props();
      expect(todoInputProps.type).toEqual('text');

    })
  })

})