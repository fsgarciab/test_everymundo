import React from 'react'
import { shallow,mount } from 'enzyme'
import {InputCoupleOriginDestiny} from '../';
import {Field} from 'redux-form';
function setup() {
  const props = {

    style:{padding:"10px"},
    getSuggestionsBySearch: jest.fn(),
    items:[]
    }

  const enzymeWrapper = shallow(<InputCoupleOriginDestiny {...props} />)


  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('InputCoupleOriginDestiny', () => {
    it('should render self ', () => {
      const { enzymeWrapper } = setup()
      expect(enzymeWrapper.find('div').first().hasClass('inputCoupleOriginDestiny')).toBe(true);
      expect(enzymeWrapper.find(Field)).toHaveLength(2);
     
    })
  })

})