import React from 'react'
import { mount } from 'enzyme'
import {InputAutocompleter} from '../';
import Autocomplete from 'react-autocomplete';

function setup() {
  const props = {
    input: jest.fn(),
    label: 'Label Test',
    items: [{name:'Arcansas (ARC)'}],
    getSuggestionsBySearch: jest.fn()
  }

  const enzymeWrapper = mount(<InputAutocompleter {...props} />)



  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('ButtonControl', () => {
    it('should render self and subcomponets', () => {
      const { enzymeWrapper } = setup()
      expect(enzymeWrapper.find('div').first().hasClass('inputAutocompleter')).toBe(true);
      enzymeWrapper.find(Autocomplete).simulate('keydown', { which: 'a' });
      expect(enzymeWrapper.find(Autocomplete)).toHaveLength(1);
      // expect(enzymeWrapper.find('input').props()["aria-expanded"]).to.equal('true');

    })
  })

})