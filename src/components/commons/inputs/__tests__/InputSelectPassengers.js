import React from 'react'
import { shallow,mount } from 'enzyme'
import {InputSelectPassengers, InputNumberCounter} from '../';
import Dropdown, { DropdownTrigger, DropdownContent } from 'react-simple-dropdown';
function setup() {
  const props = {
    className: 'newClass',
    formName: 'formName Test',
    changeValue: jest.fn(),
    passengers_adults: 1,
    passengers_children:3,
    passengers_infants:2,
    style:{
      padding:'30px'
    }
  }

  const enzymeWrapper = shallow(<InputSelectPassengers {...props} />)


  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('InputNumberCounter', () => {
    it('should render self ', () => {
      const { enzymeWrapper } = setup()
      expect(enzymeWrapper.find('div').first().hasClass('inputSelectPassengers')).toBe(true);
      expect(enzymeWrapper.find(Dropdown)).toHaveLength(1);
      expect(enzymeWrapper.find(DropdownTrigger)).toHaveLength(1);
      expect(enzymeWrapper.find(DropdownContent)).toHaveLength(1);
      expect(enzymeWrapper.find(InputNumberCounter)).toHaveLength(3);
    })
  })

})