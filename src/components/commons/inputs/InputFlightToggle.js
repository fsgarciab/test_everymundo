import React, {Component} from 'react';
import './InputFlightToggle.css';


class InputFlightToggle extends Component{
	constructor(props) {
	  super(props);
	}

	render (){

		let { className, input, label, onfocus } = this.props;
		let classes = [];
		classes.push(className);
		if (input.checked){
			classes.push("has-value");
		}

		return (
			<div className={ ['inputFlightToggle', ...classes].join(' ')}>
				<input {...input} type="checkbox" onFocus={onfocus} id="FlightRoundtrip"/>
				<label htmlFor="FlightRoundtrip">{label}
				<span className="em-flight_toggle--single"></span>
				<span className="em-flight_toggle--return"></span>
				</label>
			</div>
			)
	}

}

export {InputFlightToggle};