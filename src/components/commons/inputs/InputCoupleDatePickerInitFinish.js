import React, {Component} from 'react';
import PropTypes from 'prop-types';
import momentPropTypes from 'react-moment-proptypes';
import { forbidExtraProps } from 'airbnb-prop-types';
import moment from 'moment';
import { Field, change} from 'redux-form';
import omit from 'lodash/omit';
import {DayPickerRangeController , DayPickerSingleDateController} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import ThemedStyleSheet from 'react-with-styles/lib/ThemedStyleSheet';
import aphroditeInterface from 'react-with-styles-interface-aphrodite';
import DefaultTheme from 'react-dates/lib/theme/DefaultTheme';
import {InputText , InputFlightToggle} from './';
import './InputCoupleDatePickerInitFinish.css';

ThemedStyleSheet.registerInterface(aphroditeInterface);
ThemedStyleSheet.registerTheme(DefaultTheme);


const START_DATE = 'startDate';
const END_DATE = 'endDate';
const HORIZONTAL_ORIENTATION = 'horizontal';

const propTypes = forbidExtraProps({
  // example props for the demo
  autoFocusEndDate: PropTypes.bool,
  initialStartDate: momentPropTypes.momentObj,
  initialEndDate: momentPropTypes.momentObj,

  keepOpenOnDateSelect: PropTypes.bool,
  minimumNights: PropTypes.number,
  isOutsideRange: PropTypes.func,
  isDayBlocked: PropTypes.func,
  isDayHighlighted: PropTypes.func,

  // DayPicker props
  enableOutsideDays: PropTypes.bool,
  numberOfMonths: PropTypes.number,
  withPortal: PropTypes.bool,
  initialVisibleMonth: PropTypes.func,
  renderCalendarInfo: PropTypes.func,

  navPrev: PropTypes.node,
  navNext: PropTypes.node,

  onPrevMonthClick: PropTypes.func,
  onNextMonthClick: PropTypes.func,
  onOutsideClick: PropTypes.func,
  renderCalendarDay: PropTypes.func,
  renderDayContents: PropTypes.func,

  // i18n
  monthFormat: PropTypes.string,

  isRTL: PropTypes.bool,
  style:PropTypes.object,
  changeValue: PropTypes.func,
  formName: PropTypes.string,
  flight_toggle: PropTypes.bool
});

const defaultProps = {
  // example props for the demo
  autoFocusEndDate: false,
  initialStartDate: null,
  initialEndDate: null,

  // day presentation and interaction related props
  renderCalendarDay: undefined,
  renderDayContents: null,
  minimumNights: 1,
  isDayBlocked: () => false,
  isOutsideRange: ()=>false,
  isDayHighlighted: () => false,
  enableOutsideDays: false,

  // calendar presentation and interaction related props
  orientation: HORIZONTAL_ORIENTATION,
  withPortal: false,
  initialVisibleMonth: null,
  numberOfMonths: 2,
  onOutsideClick() {},
  keepOpenOnDateSelect: false,
  renderCalendarInfo: null,
  isRTL: false,

  // navigation related props
  navPrev: null,
  navNext: null,
  onPrevMonthClick() {},
  onNextMonthClick() {},

  // internationalization
  monthFormat: 'MMMM YYYY',
};


class InputCoupleDatePickerInitFinish extends Component {
  constructor(props) {
    super(props);

    this.state = {
      focusedInput: props.autoFocusEndDate ? END_DATE : START_DATE,
      startDateObject: props.initialStartDate,
      endDateObject: props.initialEndDate,
      shouldRenderCalendar: false
    };

    this.onDatesChange = this.onDatesChange.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    this.onFocusInput = this.onFocusInput.bind(this);
  }

  onDatesChange({ startDate, endDate }) {
    const {changeValue,formName} = this.props;
    this.setState({ startDateObject:startDate, endDateObject:endDate });
    changeValue(formName,'startDate',startDate && startDate.format('YYYY-MM-DD'));
    changeValue(formName,'endDate',endDate && endDate.format('YYYY-MM-DD'));
    if (endDate!=null){
      this.setState({shouldRenderCalendar:false});
    }

  }

  onDateChange(startDate) {
    const {changeValue,formName} = this.props;
    this.setState({ startDateObject:startDate});
    changeValue(formName,'startDate',startDate && startDate.format('YYYY-MM-DD'));
    if (startDate!=null){
      this.setState({shouldRenderCalendar:false});
    }

  }

  onFocusChange(focusedInput) {
    this.setState({
      // Force the focusedInput to always be truthy so that dates are always selectable
      focusedInput: !focusedInput ? START_DATE : focusedInput,
    });
  }

  onFocusInput(inputName,entered){
    if(entered){
      this.setState({shouldRenderCalendar:true});
    }
  }

  render() {
    const { formName, style , flight_toggle } = this.props;
    const { focusedInput,startDateObject, endDateObject} = this.state;


    const props = omit(this.props, [
      'autoFocus',
      'autoFocusEndDate',
      'initialStartDate',
      'initialEndDate',
      'formName',
      'style',
      'flight_toggle'
    ]);
    let labelStartDate = '';
    if(flight_toggle){
      labelStartDate = 'Outbound flight';
    }else{
      labelStartDate = 'One way';
    }



    return (
      <div className="inputCoupleDatePickerInitFinish" style={style}>

          <div className="em-display-flex">
            <Field name="startDate"component={InputText} type="text" label={labelStartDate} onfocus={()=>{ this.onFocusInput('startDate',true)}}  />
            <Field name="endDate"component={InputText} type="text" label="Return flight" onfocus={()=>{ this.onFocusInput('startDate',true)}} disabled={!flight_toggle}  />
            <div className="em-icon">
                <Field 
                  name = "flight_toggle" 
                  type = "checkbox"
                  component = {InputFlightToggle}
                  label = "type"/>
            </div>
          </div>
          { this.state.shouldRenderCalendar && flight_toggle &&
            <div className="em-calendar-date-picker">
            <DayPickerRangeController
              {...props}
              onDatesChange={this.onDatesChange}
              onFocusChange={this.onFocusChange}
              focusedInput={focusedInput}
              startDate={startDateObject}
              endDate={endDateObject}
            />
          </div>

          }
          {
            this.state.shouldRenderCalendar && !flight_toggle &&
             <div className="em-calendar-date-picker">
              <DayPickerSingleDateController
                {...props}
                onDateChange={this.onDateChange}
                onFocusChange={()=>{}}
                focused={false}
                date={startDateObject}
              />
            </div>
          }
          
      </div>
    );
  }
}


InputCoupleDatePickerInitFinish.propTypes = propTypes;
InputCoupleDatePickerInitFinish.defaultProps = defaultProps;

export {InputCoupleDatePickerInitFinish};