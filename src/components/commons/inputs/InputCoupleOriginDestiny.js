import React, {Component} from 'react';
import { Field } from 'redux-form';
import { InputAutocompleter } from './';
import './InputCoupleOriginDestiny.css';

class InputCoupleOriginDestiny extends Component{

	render(){
		const {style, getSuggestionsBySearch, items} = this.props;
		return (
		<div className = "inputCoupleOriginDestiny em-display-flex" style={style}>
			<Field
		        name="from"
		        type="select"
		        component={InputAutocompleter}
		        label="From"
		        getSuggestionsBySearch={ getSuggestionsBySearch}
		        items = {items}
		      />
		      <Field
		        name="to"
		        type="select"
		        component={InputAutocompleter}
		        label="To"
		        getSuggestionsBySearch={getSuggestionsBySearch}
		        items = {items}
		      />
		      <div className="em-icon">
		      	<div className="em-icon-flight">
		      	</div>
		      </div>
	     </div>
	     )
	}

}
export {InputCoupleOriginDestiny}