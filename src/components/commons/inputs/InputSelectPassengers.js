import React ,  {Component} from 'react';

import Dropdown, { DropdownTrigger, DropdownContent } from 'react-simple-dropdown';
import { InputNumberCounter } from './';
import 'react-simple-dropdown/styles/Dropdown.css';
import './InputSelectPassengers.css';

class InputSelectPassengers extends Component {
  constructor (props) {
    super(props);

    this.handleLinkClick = this.handleLinkClick.bind(this);
  }

  handleLinkClick () {
    this.refs.dropdown.hide();
  }

  render () {
    const { className , formName, changeValue, passengers_adults, passengers_children , passengers_infants, style} = this.props;

    let classes = [];
  	classes.push(className);
  	classes.push("has-value");

    let label_adults = 'Adults';
    let label_children = 'Children';
    let label_infants = 'Infants';
    let label_total = 'Passengers'
    if(passengers_adults==1){
      label_adults= 'Adult';
    }

    if(passengers_children==1){
      label_children= 'Child';
    }

    if(passengers_infants==1){
      label_infants= 'Infant';
    }

    if(passengers_adults + passengers_children + passengers_infants == 1){
      label_total= 'Passenger';
    }


    return (
    	<div className={ ['inputSelectPassengers', ...classes].join(' ')} style={style}>
		      <Dropdown className="em-passenger-dropdown" ref="dropdown">
		        <DropdownTrigger>
		          <p> {passengers_adults + passengers_children + passengers_infants} {label_total}</p>
		          <label> {passengers_adults} {label_adults}, {passengers_children} {label_children}, {passengers_infants} {label_infants}</label>
		        </DropdownTrigger>
		        <DropdownContent> 	
		            	<InputNumberCounter formName={formName} changeValue={changeValue} selector={passengers_adults} label="Adults" sublabel="12+ years" name = "passengers_adults" min="1" max="40" />
                  <InputNumberCounter formName={formName} changeValue={changeValue} selector={passengers_children} label="Children" sublabel="2-11 years" name = "passengers_children" min="0" max="40" />
                  <InputNumberCounter formName={formName} changeValue={changeValue} selector={passengers_infants} label="Infants" sublabel="0-1 year" name = "passengers_infants" min="0" max="40" />
                  <div className="em-close-button">
                  <a onClick={this.handleLinkClick.bind(this)}>Close</a>
                  </div>
		        </DropdownContent>
		      </Dropdown>
      	</div>
    );
  }
}

InputSelectPassengers.propTypes = {
  
};

export {InputSelectPassengers};