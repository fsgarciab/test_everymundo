import React, {Component} from 'react';
import Autocomplete from 'react-autocomplete';
import axios from 'axios';

import './InputAutocompleter.css';



class InputAutocompleter extends Component{
	constructor(props) {
	  super(props);
	  this.state = {
	    search : '',
	    items:[]
	  };
	}

	getSuggestions(value){
		const { getSuggestionsBySearch } = this.props;
		getSuggestionsBySearch(value);
	}
	handleOnBlur(){
		const {input} = this.props; 
		if ( this.state.search != input.value){
			input.onChange(null);
			this.setState({search:''});
		} 
	
	}

	render (){

		let { className, input, label, type, items } = this.props;
		let classes = [];
		classes.push(className);
		if (this.state.search.length>0){
			classes.push("has-value");
		}

		return (
			<div className={ ['inputAutocompleter', ...classes].join(' ')}>
				<Autocomplete
				  getItemValue={(item) => item.name}
				  items={items}
				  renderItem={(item, isHighlighted) =>
				    <div style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
				      {item.name}
				    </div>
				  }
				  value={this.state.search}
				  onChange={(event, value) => {
			            this.setState({ search: value })
			            this.getSuggestions(value);
			          }}
				  onSelect={(value,item)=>{
				  		input.onChange(value);
				  	 	this.setState({ search: value })

				  }}
				  inputProps={{
				  	onBlur:this.handleOnBlur.bind(this)
				  }}
				  wrapperStyle = {{
					 position: 'relative',
					 zIndex:2
				  }}
					
				/>
				<label>{label}</label>
			</div>
			)
	}

}

export {InputAutocompleter};