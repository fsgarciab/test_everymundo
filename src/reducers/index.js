import {combineReducers} from 'redux';
import FlightReducer from './FlightReducer';
import { reducer as formReducer } from 'redux-form'

export default combineReducers({
	flightData: FlightReducer,
	form: formReducer
});