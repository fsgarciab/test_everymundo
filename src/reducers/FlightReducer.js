import { REDIRECT_SEARCH_INIT, SUGGESTIONS_SEARCH_INIT,SUGGESTIONS_SEARCH_FINISHED } from '../components/Flight/FlightActions';


export default (state = {
		isLoading: true,
		isRedirecting: false,
		data:[],
		errorData:{},
		error: false
	}, action) => {
	switch	(action.type){
		case REDIRECT_SEARCH_INIT:
			 return Object.assign({}, state, {isRedirecting:true });
		case SUGGESTIONS_SEARCH_INIT:
			 return Object.assign({}, state, {isLoading: true,isRedirecting:false, data: []});
		 case SUGGESTIONS_SEARCH_FINISHED:
			 return Object.assign({}, state, {isLoading: false,isRedirecting:false, data: action.payload});
		default:
			return state;
	}
}