import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { connect } from 'react-redux';
import BookingBarContainer from './components/BookingBar/BookingBarContainer'
import './layout/generic-layout.css';

class App extends Component {
  render() {
    return (
        <div class="app-bg">
          <BookingBarContainer />
        </div>
    );
  }
}

export default App;
