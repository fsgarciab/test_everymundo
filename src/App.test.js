
import React from 'react';
import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk';
import App from './App';
import {Provider} from 'react-redux';
import BookingBarContainer from './components/BookingBar/BookingBarContainer';

describe('components', () => {
  describe('App', () => {
    it('should render self', () => {
      const wrapper = shallow(<App />);
      const bookingBarContainer = <BookingBarContainer/>;
      expect(wrapper.contains(bookingBarContainer)).toEqual(true);
    })
  })
})